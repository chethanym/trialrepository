#import pymysql
import os
import csv
import html
import smtplib
import subprocess
import os.path
from os import path
import configparser
from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config1, read_db_config2, read_output_config2, read_output_config1
import re
 
def query_Sentry_URI_source(query1, file1, header1):
    dbconfig = read_db_config1()
    conn = MySQLConnection(**dbconfig)
    cursor = conn.cursor()
    try:
        iterable = cursor.execute(query1, multi=True)
        field = cursor.description
        #row = cursor.fetchall()
       # iterable = cursor.execute(sql1,multi=True)
        for item in iterable:
            row=item.fetchall()
        print file1
        fp = open(file1, 'a+')
        fp.write(header1 + "\n")
        attach_file = csv.writer(fp)
        #attach_file.writerows(field)
        attach_file.writerows(row)
        fp.close()
        print "The file has been written to: " + file1
    except Error as e:
        print(e)
 
    finally:
        cursor.close()
        conn.close()

def query_Sentry_URI_destination(query1, file1, header1):
    dbconfig = read_db_config2()
    conn = MySQLConnection(**dbconfig)
    cursor = conn.cursor()
    try:
        iterable = cursor.execute(query1, multi=True)
        field = cursor.description
        #row = cursor.fetchall()
       # iterable = cursor.execute(sql1,multi=True)
        for item in iterable:
            row=item.fetchall()
        print file1
        fp = open(file1, 'a+')
        fp.write(header1 + "\n")
        attach_file = csv.writer(fp)
        #attach_file.writerows(field)
        attach_file.writerows(row)
        fp.close()
        print "The file has been written to: " + file1
    except Error as e:
        print(e)

    finally:
        cursor.close()
        conn.close()

def compare_sentry_grants():
    with open('/home/chethanym/sentry-operations/sentry_uri_DB_source_results.csv', 'r') as t1, open('/home/chethanym/sentry-operations/sentry_uri_DB_destination_results.csv', 'r') as t2:
      fileone = t1.readlines()
      filetwo = t2.readlines()

    with open('grants_differences.csv', 'w') as outFile:
      for line in filetwo:
          if line not in fileone:
              outFile.write(line)
    print "grants_differences.csv file is craeted with the grants differences" 
    
def grant_scripts():
        os.system("awk -F',' '{print $1}' grants_differences.csv | uniq > tmp.txt");
        os.system('''awk '{print "CREATE ROLE "$1";"}' tmp.txt > grant_scripts.csv''');
        os.system('''awk 'BEGIN {FS=","};{print "GRANT ROLE "$1" TO GROUP "$2";"}' grants_differences.csv | uniq >> grant_scripts.csv''');
        os.system('''awk 'BEGIN {FS=","};{if ($3 == "TABLE") {print "GRANT "$(NF-1)" ON "$3" "$6" TO ROLE "$1";"} else {print "GRANT "$(NF-1)" ON "$3" "$5" TO ROLE "$1";"}}' grants_differences.csv | sed -e 's/*/ALL/' -e 's/all/ALL/' >> grant_scripts.csv''');

        print "grant_scripts.csv file is created with all create and grant commands"
        os.remove("tmp.txt");

def apply_grants():
    config=configparser.ConfigParser()
    config.read('config1.ini')
    impala_daemon=config.get('impala_config','host')
    ca_cert=config.get('impala_config','ca_cert')
    print impala_daemon
    cmd="impala-shell -i "+impala_daemon+" -d default -k --ssl --ca_cert="+ca_cert+" -f grant_scripts.csv";
    os.system(cmd);
    #print (impala_daemon,ca_cert)

def sentry_menu():
    menuloop = 1
    choice = 0
    path1 = read_output_config1()
    path2 = read_output_config2()

    file3 = path1.get('dir1')
    file4 = path2.get('dir1')

    file2 = os.path.expanduser(file3)
    if not os.path.exists(file2):
      os.makedirs(file2)

    file5 = os.path.expanduser(file4)
    if not os.path.exists(file5):
      os.makedirs(file5)
    while menuloop == 1:

      print("""
          1-Create Sentry URI and DATABASE CSV Report for Source cluster
          2-Create Sentry URI and DATABASE CSV Report for Destination cluster
          3-Create CSV report for missing grants from the cluster
          4-create roles and grants commands
          5-Apply grants to the cluster
          6-Exit
       """)

      choice = int(input("Choose your option: "))

      if choice == 1:
          query1 = ("SELECT r.ROLE_NAME,a.GROUP_NAME,p.PRIVILEGE_SCOPE,p.SERVER_NAME,p.URI,p.ACTION,p.WITH_GRANT_OPTION FROM SENTRY_ROLE_DB_PRIVILEGE_MAP AS m JOIN SENTRY_ROLE AS r ON m.ROLE_ID=r.ROLE_ID JOIN SENTRY_ROLE_GROUP_MAP AS c ON r.ROLE_ID=c.ROLE_ID JOIN SENTRY_GROUP AS a ON c.GROUP_ID=a.GROUP_ID JOIN SENTRY_DB_PRIVILEGE AS p ON m.DB_PRIVILEGE_ID=p.DB_PRIVILEGE_ID WHERE p.PRIVILEGE_SCOPE = 'URI'")
          file1 = file2 + "sentry_uri_DB_source_results.csv"
          header1 = ("ROLE_NAME,GROUP_NAME,PRIVILEGE_SCOPE,SERVER_NAME,URI,ACTION,WITH_GRANT_OPTION")
          query_Sentry_URI_source(query1,file1,header1)
 
          query1 = ("SELECT r.ROLE_NAME,a.GROUP_NAME,p.PRIVILEGE_SCOPE,p.SERVER_NAME,p.DB_NAME,p.TABLE_NAME,p.COLUMN_NAME,p.ACTION,p.WITH_GRANT_OPTION FROM SENTRY_ROLE_DB_PRIVILEGE_MAP AS m JOIN SENTRY_ROLE AS r ON m.ROLE_ID=r.ROLE_ID JOIN SENTRY_ROLE_GROUP_MAP AS c ON r.ROLE_ID=c.ROLE_ID JOIN SENTRY_GROUP AS a ON c.GROUP_ID=a.GROUP_ID JOIN SENTRY_DB_PRIVILEGE AS p ON m.DB_PRIVILEGE_ID=p.DB_PRIVILEGE_ID WHERE p.PRIVILEGE_SCOPE IN ('DATABASE','TABLE')")
          file1 = file2 + "sentry_uri_DB_source_results.csv"
          header1 = ("ROLE_NAME,GROUP_NAME,PRIVILEGE_SCOPE,SERVER_NAME,DB_NAME,TABLE_NAME,COLUMN_NAME,ACTION,WITH_GRANT_OPTION")
          query_Sentry_URI_source(query1,file1,header1) 
      
      if choice == 2:
          query1 = ("SELECT r.ROLE_NAME,a.GROUP_NAME,p.PRIVILEGE_SCOPE,p.SERVER_NAME,p.URI,p.ACTION,p.WITH_GRANT_OPTION FROM SENTRY_ROLE_DB_PRIVILEGE_MAP AS m JOIN SENTRY_ROLE AS r ON m.ROLE_ID=r.ROLE_ID JOIN SENTRY_ROLE_GROUP_MAP AS c ON r.ROLE_ID=c.ROLE_ID JOIN SENTRY_GROUP AS a ON c.GROUP_ID=a.GROUP_ID JOIN SENTRY_DB_PRIVILEGE AS p ON m.DB_PRIVILEGE_ID=p.DB_PRIVILEGE_ID WHERE p.PRIVILEGE_SCOPE = 'URI'")
          file6 = file5 + "sentry_uri_DB_destination_results.csv"
          header1 = ("ROLE_NAME,GROUP_NAME,PRIVILEGE_SCOPE,SERVER_NAME,URI,ACTION,WITH_GRANT_OPTION")
          query_Sentry_URI_destination(query1,file6,header1)

          query1 = ("SELECT r.ROLE_NAME,a.GROUP_NAME,p.PRIVILEGE_SCOPE,p.SERVER_NAME,p.DB_NAME,p.TABLE_NAME,p.COLUMN_NAME,p.ACTION,p.WITH_GRANT_OPTION FROM SENTRY_ROLE_DB_PRIVILEGE_MAP AS m JOIN SENTRY_ROLE AS r ON m.ROLE_ID=r.ROLE_ID JOIN SENTRY_ROLE_GROUP_MAP AS c ON r.ROLE_ID=c.ROLE_ID JOIN SENTRY_GROUP AS a ON c.GROUP_ID=a.GROUP_ID JOIN SENTRY_DB_PRIVILEGE AS p ON m.DB_PRIVILEGE_ID=p.DB_PRIVILEGE_ID WHERE p.PRIVILEGE_SCOPE IN ('DATABASE','TABLE')")
          file6 = file5 + "sentry_uri_DB_destination_results.csv"
          header1 = ("ROLE_NAME,GROUP_NAME,PRIVILEGE_SCOPE,SERVER_NAME,DB_NAME,TABLE_NAME,COLUMN_NAME,ACTION,WITH_GRANT_OPTION")
          query_Sentry_URI_destination(query1,file6,header1)     

      if choice == 3:
          compare_sentry_grants();

      if choice == 4:
          grant_scripts();

      if choice == 5:
          grants=raw_input("Do you really want to apply grants to the cluster? yes/no ")
          print grants
          if grants=="yes":
              apply_grants();
 
      elif choice == 6:
          menuloop = 0

    print("Thanks!")
 
 
if __name__ == '__main__':
    #query_Sentry_URI()
    sentry_menu()
